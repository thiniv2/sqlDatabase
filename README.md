# Data Persistence and Access

## Features

SuperHeroesDb:
- Create database called SuperHeroesDb
- Create tables Superhero, power and assistant
- Create relationship between the tables
- Insert data into tables
- Update data in tables
- Delete data from tables

Chinook:
- Read all customers
- Read specific customers
- Browse customers page
- Add new customer
- Update customer
- Display number of customers in each country
- Display customers with highest spenders
- Display given customers popular genre(s)

## Contributors
Thien Nguyen <br />
Laura Koivuranta
