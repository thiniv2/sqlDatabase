﻿namespace Chinook.Models
{
    public class CustomerGenre
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
    }
}
