﻿namespace Chinook.Models
{
    public class CustomerTopSpender
    {
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SpentMoney { get; set; }
    }
}

