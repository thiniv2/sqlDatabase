﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.ALT_Repositories
{
	public interface IRepository<T>
	{
		T GetById(string id);
		IEnumerable<T> GetAll();
		bool Add(T entity);
		bool Edit(T entity);
		bool Delete(T entity);
	}
}
