﻿using Chinook.Models;
using Chinook.Repositories;
using Microsoft.Data.SqlClient;

namespace sqlDatabase.Chinook.ALT_Repositories
{
	public class CustomerRepositoryImpl : ICustomerRepositoryImpl
	{
		public List<CustomerCountry> GetAllCountries()
		{
			List<CustomerCountry> countryList = new List<CustomerCountry>();
			string sql = $"SELECT Country, COUNT (*) AS Number FROM Customer GROUP BY Country ORDER BY Number DESC";
			try
			{
				// Connect
				using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
				{
					conn.Open();
					// Make a command
					using (SqlCommand cmd = new SqlCommand(sql, conn))
					{
						// Reader
						using (SqlDataReader reader = cmd.ExecuteReader())
						{
							while (reader.Read())
							{
								// Handle result
								CustomerCountry temp = new CustomerCountry();
								temp.Country = reader.GetString(0);
								temp.Count = reader.GetInt32(1);
								countryList.Add(temp);
							}
						}
					}
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine("Error cannot get country list\n" + e);
			}
			return countryList;
		}

		public List<CustomerGenre> GetCustomerGenres(string customerId)
		{
            List<CustomerGenre> genreList = new List<CustomerGenre>();
            string sql = $"WITH TempTable AS" +
				$"(SELECT TempData.CustomerId, TempData.FirstName, TempData.LastName, TempData.Name, TempData.Count " +
				$"FROM (SELECT Invoice.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name, COUNT (*) AS Count " +
				$"FROM Customer, Invoice, InvoiceLine, Track, Genre " +
				$"WHERE Customer.CustomerId = {customerId} " +
				$"AND Invoice.InvoiceId = InvoiceLine.InvoiceId " +
				$"AND Invoice.CustomerId = Customer.CustomerId " +
				$"AND InvoiceLine.TrackId = Track.TrackId " +
				$"AND Track.GenreId = Genre.GenreId " +
				$"GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name) AS TempData) " +
				$"SELECT CustomerId, Firstname + ' ' + LastName AS CustomerName, Name AS Genre, Count FROM TempTable " +
				$"WHERE Count = (SELECT MAX(Count) FROM TempTable)";

            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
								// Handle result
								CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.Name = reader.GetString(1);
								temp.Genre = reader.GetString(2);
								genreList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error cannot get genres\n");
            }
            return genreList;
        }

		public List<CustomerTopSpender> TopSpenders(string offset)
		{
            List<CustomerTopSpender> spenderList = new List<CustomerTopSpender>();
            string sql = $"SELECT Customer.CustomerId, " +
				$"FirstName, LastName, sum (Invoice.Total) " +
				$"AS Sum FROM Customer, Invoice " +
				$"WHERE Customer.CustomerId = Invoice.CustomerId " +
                $"GROUP BY Customer.CustomerId, FirstName, LastName ORDER BY Sum DESC OFFSET {offset} " +
				$"ROWS FETCH NEXT 10 ROWS ONLY";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
								// Handle result
								CustomerTopSpender temp = new CustomerTopSpender();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
								temp.LastName = reader.GetString(2);
								temp.SpentMoney = reader.GetDecimal(3).ToString();
								spenderList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Error cannot get Top Spenders\n");
            }
            return spenderList;
        }
	}
}
