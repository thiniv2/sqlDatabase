﻿using Chinook.Models;

namespace sqlDatabase.Chinook.ALT_Repositories
{
	public interface ICustomerRepositoryImpl
	{
		public List<CustomerCountry> GetAllCountries();
		public List<CustomerTopSpender> TopSpenders(string offset);
        public List<CustomerGenre> GetCustomerGenres(string customerId);
    }
}
