﻿using Chinook.Models;

namespace Chinook.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(string id);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetAllCustomersByName(string name);
        public List<Customer> GetAllCustomersByPage(string offset);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer, string Id);
    }
}
