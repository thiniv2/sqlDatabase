﻿using System.Data;
using System.Security.Claims;
using Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
	public class CustomerRepository : ICustomerRepository
	{
		public List<Customer> GetAllCustomers()
		{
			List<Customer> customersList = new List<Customer>();
			string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
			try
			{
				// Connect
				using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
				{
					conn.Open();
					// Make a command
					using (SqlCommand cmd = new SqlCommand(sql, conn))
					{
						// Reader
						using (SqlDataReader reader = cmd.ExecuteReader())
						{
							while (reader.Read())
							{
								// Handle result
								Customer temp = new Customer();
								temp.CustomerId = reader.GetInt32(0).ToString();
								temp.FirstName = reader.GetString(1);
								temp.LastName = reader.GetString(2);
								temp.Country = reader.IsDBNull(3) ? "<null>" : reader.GetString(3);
								temp.PostalCode = reader.IsDBNull(4) ? "<null>" : reader.GetString(4);
								temp.PhoneNumber = reader.IsDBNull(5) ? "<null>" : reader.GetString(5);
								temp.Email = reader.GetString(6);
								customersList.Add(temp);
							}
						}
					}
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine("Error in get all customers\n" + e);
			}
			return customersList;
		}
		public List<Customer> GetAllCustomersByName(string name)
		{
			List<Customer> customersList = new List<Customer>();
			string sql = $"SELECT * FROM Customer WHERE FirstName LIKE '%{name}%'";
			try
			{
				// Connect
				using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
				{
					conn.Open();
					// Make a command
					using (SqlCommand cmd = new SqlCommand(sql, conn))
					{
						// Reader
						using (SqlDataReader reader = cmd.ExecuteReader())
						{
							while (reader.Read())
							{
								// Handle result
								Customer temp = new Customer();
								temp.CustomerId = reader.GetInt32(0).ToString();
								temp.FirstName = reader.GetString(1);
								temp.LastName = reader.GetString(2);
								temp.Country = reader.IsDBNull(3) ? "<null>" : reader.GetString(3);
								temp.PostalCode = reader.IsDBNull(4) ? "<null>" : reader.GetString(4);
								temp.PhoneNumber = reader.IsDBNull(5) ? "<null>" : reader.GetString(5);
								temp.Email = reader.IsDBNull(6) ? "<null>" : reader.GetString(6);
								customersList.Add(temp);
							}
						}
					}
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine("Error cant find customers by name");
			}
			return customersList;
		}
		public List<Customer> GetAllCustomersByPage(string offset)
		{
			List<Customer> customersList = new List<Customer>();
			string sql = $"SELECT * FROM Customer ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT 10 ROWS ONLY";
			try
			{
				// Connect
				using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
				{
					conn.Open();
					// Make a command
					using (SqlCommand cmd = new SqlCommand(sql, conn))
					{
						// Reader
						using (SqlDataReader reader = cmd.ExecuteReader())
						{
							while (reader.Read())
							{
								// Handle result
								Customer temp = new Customer();
								temp.CustomerId = reader.GetInt32(0).ToString();
								temp.FirstName = reader.GetString(1);
								temp.LastName = reader.GetString(2);
								temp.Country = reader.IsDBNull(3) ? "<null>" : reader.GetString(3);
								temp.PostalCode = reader.IsDBNull(4) ? "<null>" : reader.GetString(4);
								temp.PhoneNumber = reader.IsDBNull(5) ? "<null>" : reader.GetString(5);
								temp.Email = reader.IsDBNull(6) ? "<null>" : reader.GetString(6);
								customersList.Add(temp);
							}
						}
					}
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine("Error cant find customers by page");
			}
			return customersList;
		}
		public Customer GetCustomer(string id)
		{
			Customer customer = new Customer();
			string sql = "SELECT CustomerId, " +
				"FirstName, " +
				"LastName, " +
				"Country FROM Customer " +
				"WHERE CustomerId = @CustomerId";

			try
			{   // Connect
				using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
				{
					conn.Open();
					// Make a command
					using (SqlCommand cmd = new SqlCommand(sql, conn))
					{
						// Reader
						cmd.Parameters.AddWithValue("@CustomerId", id);
						using (SqlDataReader reader = cmd.ExecuteReader())
						{
							while (reader.Read())
							{
								// Handle result
								customer.CustomerId = reader.GetInt32(0).ToString();
								customer.FirstName = reader.GetString(1);
								customer.LastName = reader.GetString(2);
								customer.Country = reader.GetString(3);
							}
						}
					}
				}
			}
			catch (SqlException ex)
			{
				//Log
				Console.WriteLine("could not get customer");
			}
			return customer;
		}
		public bool AddNewCustomer(Customer customer)
		{
			bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                         "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
			try
			{
				using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
				{
					conn.Open();
					using (SqlCommand cmd = new SqlCommand(sql, conn))
					{
						cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
						cmd.Parameters.AddWithValue("@LastName", customer.LastName);
						cmd.Parameters.AddWithValue("@Country", customer.Country);
						cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
						cmd.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
						cmd.Parameters.AddWithValue("@Email", customer.Email);
						success = cmd.ExecuteNonQuery() > 0 ? true : false;
					}
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine("Can't add a new customer");
			}
			return success;
		}
		public bool UpdateCustomer(Customer customer, string Id)
		{
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                         "PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                         "WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
						cmd.Parameters.AddWithValue("@CustomerId", Id);
						cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine("Can't update customer");
				Console.WriteLine(e);
            }
            return success;
        }
	}
}
