﻿using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public class ConnectionStringHelper
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = @".\SQLEXPRESS"; 
			connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
			connectionStringBuilder.Encrypt = false;
			return connectionStringBuilder.ConnectionString;
        }
    }
}
