﻿using Azure.Core;
using Chinook.Models;
using Chinook.Repositories;
using sqlDatabase.Chinook.ALT_Repositories;
using System.Numerics;
using System.Text;

internal class Program
{
	private static void Main(string[] args)
	{
		ICustomerRepository repository = new CustomerRepository();
		ICustomerRepositoryImpl customerRepositoryImpl = new CustomerRepositoryImpl();
		while (true)
		{
			Console.WriteLine("Choose Action:\n" +
				"(1) Select all\n" +
				"(2) Select one\n" +
				"(3) Search customer by name\n" +
                "(4) Customers page\n" +
				"(5) Add a new customer\n" +
                "(6) Update a customer\n" +
                "(7) List customers by countries\n" +
				"(8) List customers by highest amount spent\n" +
				"(9) List customer's most popular genre(s) by ID\n" +
				"Type 'exit' to exit");
			var input = Console.ReadLine();
			switch (input)
			{
				case "1":
					TestSelectAll(repository);
					break;
				case "2":
					Console.WriteLine("Enter customerId:");
					input = Console.ReadLine();
					TestSelect(repository, input);
					break;
				case "3":
					Console.WriteLine("Enter customer's name: ");
					input = Console.ReadLine();
					SelectAllByName(repository, input);
					break;
				case "4":
					GetCustomersPage(repository);
					break;
                case "5":
					TestInsert(repository);
					break;
                case "6":
					TestUpdate(repository);
                    break;
                case "7":
					SelectAllCountries(customerRepositoryImpl);
					break;
                case "8":
					GetSpendersPage(customerRepositoryImpl);
                    break;
				case "9":
					Console.Clear();
					Console.WriteLine("Enter customerId: ");
					input = Console.ReadLine();
					SelectAllGenres(customerRepositoryImpl, input);
					break;
				case "exit":
					Environment.Exit(0);
					break;
                default:
					break;
			}
		}
	}

	static void SelectAllGenres(ICustomerRepositoryImpl customerRepositoryImpl, string customerId)
	{
		PrintAllGenres(customerRepositoryImpl.GetCustomerGenres(customerId));
	}

	static void PrintAllGenres(IEnumerable<CustomerGenre> genres)
	{
		Console.WriteLine("|Customer Id| |Customer Name| |Music Genre|");
		foreach (var genre in genres)
		{
			PrintGenre(genre);
		}
		Console.WriteLine("\n");
	}

	static void PrintGenre(CustomerGenre genre)
	{
		if (genre.CustomerId == null)
		{
			Console.Clear();
			Console.WriteLine("Genre not found\n");
			return;
		}
		Console.WriteLine($@"{genre.CustomerId} {genre.Name} {genre.Genre}");
	}
	static void PrintAllSpenders(IEnumerable<CustomerTopSpender> spenders)
	{
		foreach (var spender in spenders)
		{
			PrintSpender(spender);
		}
	}

	static void GetSpendersPage(ICustomerRepositoryImpl customerRepositoryImpl)
	{
		Console.Clear();
		var offset = 0;
		var page = 1;
		PrintAllSpenders(customerRepositoryImpl.TopSpenders(offset.ToString()));
		while (true)
		{
			Console.WriteLine($"\nPage {page}");
			Console.WriteLine("\nPress (1) For Next Page. Press (2) For Previous Page");
			var input = Console.ReadKey();
			if (input.Key == ConsoleKey.D1)
			{
				Console.Clear();
				offset += 10;
				page++;
				PrintAllSpenders(customerRepositoryImpl.TopSpenders(offset.ToString()));
			}
			if (input.Key == ConsoleKey.D2 && page != 1)
			{
				Console.Clear();
				offset -= 10;
				page--;
				PrintAllSpenders(customerRepositoryImpl.TopSpenders(offset.ToString()));
			}
		}
	}

	static void PrintSpender(CustomerTopSpender spender)
	{
		if (spender.CustomerId == null)
		{
			Console.Clear();
			Console.WriteLine("Spender not found\n");
			return;
		}
		Console.WriteLine($"--- {spender.CustomerId} {spender.FirstName} {spender.LastName} {spender.SpentMoney}---");
	}

	static void SelectAllCountries(ICustomerRepositoryImpl customerRepositoryImpl)
	{
		PrintAllCountries(customerRepositoryImpl.GetAllCountries());
	}

	static void PrintAllCountries(IEnumerable<CustomerCountry> countries)
	{
		foreach (var country in countries)
		{
			PrintCountry(country);
		}
	}

	static void PrintCountry(CustomerCountry country)
	{
		if (country.Country == null)
		{
			Console.Clear();
			Console.WriteLine("Country not found\n");
			return;
		}
		Console.WriteLine($"--- {country.Country} {country.Count} ---");
	}

	static void GetCustomersPage(ICustomerRepository repository)
	{
		Console.Clear();
		var offset = 0;
		var page = 1;
		PrintCustomers(repository.GetAllCustomersByPage(offset.ToString()));
		while (true)
		{
			Console.WriteLine($"\nPage {page}");
			Console.WriteLine("\nPress (1) For Next Page. Press (2) For Previous Page");
			var input = Console.ReadKey();
			if (input.Key == ConsoleKey.D1)
			{
				Console.Clear();
				offset += 10;
				page++;
				PrintCustomers(repository.GetAllCustomersByPage(offset.ToString()));
			}
			if (input.Key == ConsoleKey.D2)
			{
				Console.Clear();
				offset -= 10;
				page--;
				PrintCustomers(repository.GetAllCustomersByPage(offset.ToString()));
			}
		}
	}

	static void SelectAllByName(ICustomerRepository repository, string input)
	{
		PrintCustomers(repository.GetAllCustomersByName(input));
	}

	static void TestSelectAll(ICustomerRepository repository)
	{
		PrintCustomers(repository.GetAllCustomers());
	}

	static void TestSelect(ICustomerRepository repository, string customerId)
	{
		PrintCustomer(repository.GetCustomer(customerId));
	}

	static void TestInsert(ICustomerRepository repository)
	{
		Console.WriteLine("Enter First name");
		var FirstName = Console.ReadLine();
		Console.WriteLine("Enter Last name");
		var LastName = Console.ReadLine();
		Console.WriteLine("Enter Country");
		var Country = Console.ReadLine();
		Console.WriteLine("Enter Postal code");
		var Postal = Console.ReadLine();
		Console.WriteLine("Enter Phone");
		var Phone = Console.ReadLine();
		Console.WriteLine("Enter Email");
		var Email = Console.ReadLine();

		Customer test = new Customer()
		{
			FirstName = FirstName,
			LastName = LastName,
			Country = Country,
			PostalCode = Postal,
			PhoneNumber = Phone,
			Email = Email
		};
		if (repository.AddNewCustomer(test))
		{
			Console.WriteLine("Succesful, insert works!");
			SelectAllByName(repository, FirstName);
		}
		else
		{
			Console.WriteLine("Sadge, insert didn't work.");
		}
	}

	static void TestUpdate(ICustomerRepository repository)
	{
		Console.WriteLine("Enter Id you want to update");
		var Id = Console.ReadLine();
		Console.WriteLine("Update First name");
		var FirstName = Console.ReadLine();
		Console.WriteLine("Update Last name");
		var LastName = Console.ReadLine();
		Console.WriteLine("Update Country");
		var Country = Console.ReadLine();
        Console.WriteLine("Update Postal code");
        var Postal = Console.ReadLine();
		Console.WriteLine("Update Phone");
		var Phone = Console.ReadLine();
		Console.WriteLine("Update Email");
		var Email = Console.ReadLine();

		Customer test = new Customer()
		{
			FirstName = FirstName,
			LastName = LastName,
			Country = Country,
			PostalCode = Postal,
			PhoneNumber = Phone,
			Email = Email
		};
		if (repository.UpdateCustomer(test, Id))
		{
			Console.WriteLine("Succesful, update works!");
			SelectAllByName(repository, FirstName);
		}
		else
		{
			Console.WriteLine("Sadge, update didn't work.");
		}
	}

	static void PrintCustomers(IEnumerable<Customer> customers)
	{
		foreach (Customer customer in customers)
		{
			PrintCustomer(customer);
		}
	}

	static void PrintCustomer(Customer customer)
	{
		if (customer.CustomerId == null)
		{
			Console.Clear();
			Console.WriteLine("Customer not found\n");
			return;
		}
		Console.WriteLine($"--- {customer.CustomerId} " +
			$"{customer.FirstName} {customer.LastName} " +
			$"{customer.Country} {customer.PostalCode} " +
			$"{customer.PhoneNumber} {customer.Email} ---");
	}
}