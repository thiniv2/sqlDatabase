﻿INSERT INTO Power (Name, Description)
VALUES
('Super jump', 'Jump very far.'),
('Rasengan', 'Very powerful attack.. or something.'),
('Moon Spiral Heart Attack', 'Attack or purify enemy.'),
('Power Whip', 'Whips very hard.')

INSERT INTO SuperheroPower (Superhero_id, Power_id)
VALUES
(2, 2),
(2, 1),
(3, 3),
(1, 4),
(1, 2)
